# TQPY

#### 介绍
基于天勤接口和Python的开源期货实盘交易系统
不了解天勤的开源先学习一下天勤：https://www.shinnytech.com/tianqin/

#### 软件架构
软件架构说明


#### 安装教程

1.  下载pycharm 或者别的python IDE https://www.jetbrains.com/pycharm/download/
2.  下载anaconda https://www.anaconda.com/
3.  下载天勤sdk  https://www.shinnytech.com/tianqin/



#### 使用说明
主要提供天勤在期货交易当中的多策略，多账户，多周期的的一种解决方案。

1.  首先申请天勤的账户，然后在![输入图片说明](https://images.gitee.com/uploads/images/2021/0425/123854_05e8a65d_1721305.png "屏幕截图.png") Strategy类的get_params 这个函数里面的这里面填写账户和密码![输入图片说明](https://images.gitee.com/uploads/images/2021/0425/124045_788afc6d_1721305.png "屏幕截图.png")
2.  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0425/124241_c66b3423_1721305.png "屏幕截图.png")  然后在strategies 目录下新建策略。在![输入图片说明](https://images.gitee.com/uploads/images/2021/0425/124536_a60f4685_1721305.png "屏幕截图.png") config.yml下配置策略开启。最后点击main.py  文件运行
![输入图片说明](https://images.gitee.com/uploads/images/2021/0425/130642_39e74f16_1721305.png "屏幕截图.png")


3.  基于天勤的开源期货交易系统的使用演示视频地址：https://www.zhihu.com/zvideo/1369638550170808320
    数据库版本演示：https://www.zhihu.com/zvideo/1370376761373466624

   交流可以加QQ群：334141701
4，课程地址：https://edu.csdn.net/course/detail/32594 
框架分为基础版本和升级版本，在课程当中都有介绍，升级版本主要是用数据库取代了配置文件，减少了多策略，多账户，多周期的线程数量。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
